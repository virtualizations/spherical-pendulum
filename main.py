#!/usr/bin/env python

import yaml
import unittest
import pygame
import sys

import math
from math import sin as sin
from math import cos as cos
from math import pi as PI

BLACK       = (  0,   0,   0)
SCREENX     = 640
SCREENY     = 480

class Script():
    def main(self):
        simulacion = Simulacion()

        screen = pygame.display.set_mode((SCREENX,SCREENY))
        screen.fill((255,255,255)) # Clears the screen.

        # Entering the event loop
        while True:
            #Checking for the quit event
            for evt in pygame.event.get():
                if evt.type == pygame.QUIT:
                    pygame.quit(); sys.exit()

            simulacion.tick()

            tiempo, (x,y) = simulacion.puntos[-1]

            x += SCREENX/2.0
            y += SCREENY/2.0

            puntoAsInt = [ int(round(coord)) for coord in (x,y) ]

            print(( round(tiempo,3), puntoAsInt))
            pygame.draw.circle(screen, BLACK, puntoAsInt, 1)

            pygame.display.update()

            # Esperar cien milisegundos ...
            pygame.time.delay(100)

class Simulacion():
    """Una Simulacion para contener resultados de nuestro péndulo"""
    def __init__(self):
        super(Simulacion, self).__init__()

        self.b          = 1.0
        self.g          = 9.8
        self.time       = 0.0
        self.timeDelta  = 0.2

        self.puntos     = []
        self.pendulo    = Pendulo(self.b, self.g)


        self.agregarPunto()


    def agregarPunto(self):
        self.puntos.append( (self.time, self.pendulo.posicion[1:] ) )
        # self.puntos.append( (self.time, self.pendulo.posicionArectangulares()[:2] ) )
        

    def tick(self):
        """Esta función debe actualizar los valores de posición y velocidad del péndulo"""

        # Actualizando la posición.
        oldPos      = self.pendulo.posicion[:]
        oldVel      = self.pendulo.velocidad[:]
        newPosTheta = oldPos[1] + oldVel[1] * self.timeDelta
        newPosPhi   = oldPos[2] + oldVel[2] * self.timeDelta

        self.pendulo.posicion = (oldPos[0], newPosTheta, newPosPhi)

        # Actualizando la velocidad
        oldAcc      = self.pendulo.aceleracion[:]
        newVelTheta = oldVel[1] + oldAcc[1] * self.timeDelta
        newVelPhi   = oldVel[2] + oldAcc[2] * self.timeDelta

        self.pendulo.velocidad = (oldVel[0], newVelTheta, newVelPhi)

        # Actualizando la aceleración
        self.pendulo.aceleracion = \
            ( oldAcc[0], self.pendulo.newAccTheta(), self.pendulo.newAccPhi() )


        # Agregando el punto actual en el tiempo
        self.time += self.timeDelta # Actualizando el tiempo
        self.agregarPunto()

        
class Pendulo():
    """Modelo de péndulo"""
    def __init__(self, b, g):
        super(Pendulo, self).__init__()
        # Valores iniciales
        self.longitud       = 1.0
        self.masa           = 1.0
        self.posicion       = (self.longitud, 0.5*PI, 0.0)   # r, angulo1, angulo2
        self.velocidad      = (0.0, 1.2, 1.2)   # r, angulo1, angulo2
        self.aceleracion    = (0.0, 0.1, 0.1)   # r, angulo1, angulo2

        self.gamma      = b / (2 * self.masa)
        self.w_0        = math.sqrt(g / self.longitud)


    def newAccTheta(self):
        r, theta, phi = self.posicion
        Vr, Vtheta, Vphi = self.velocidad
        return ( (Vphi**2) * cos(theta) + self.w_0**2 ) * sin(theta) - 2 * self.gamma * Vtheta

    def newAccPhi(self):
        r, theta, phi = self.posicion
        Vr, Vtheta, Vphi = self.velocidad
        sinTheta = theta if sin(theta) == 0 else sin(theta)

        return (-2*Vphi*(Vtheta*cos(theta) + self.gamma*sinTheta))/sinTheta

    def posicionArectangulares(self):
        r, theta, phi = self.posicion
        return ( r * sin(theta) * cos(phi)
                , r * sin(theta) * sin(phi)
                , r * cos(theta) )


################### Defining tests
class TestScript(unittest.TestCase):
    def test_Constructor(self):
        self.assertEqual( type(Script()), Script )


################### Launch the script
if __name__ == '__main__':
    ENV = yaml.load( open(".env", 'r') )
    Script().main()