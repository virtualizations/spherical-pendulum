#!/usr/bin/env python

import yaml
import unittest

import numpy as np
from numpy import sin
from numpy import cos
from numpy import pi as PI

from scipy.integrate import odeint

import matplotlib.pyplot as plt


class Pendulo():
    """Modelo de péndulo"""
    def __init__(self, b, g):
        super(Pendulo, self).__init__()
        self.b = b
        self.g = g
        self.longitud    = 1.0
        self.masa        = 1.0

        self.gamma      = b / (2 * self.masa)
        self.omega      = np.sqrt(g / self.longitud)

        # Sistema de ecuaciones a utilizar
        self.sistemaEq  = 'C01'
        # self.sistemaEq  = 'sinFriccion'

    def coordenadasRectangulares(self, theta, phi):
        return [self.longitud * sin(theta) * cos(phi)
            , self.longitud * sin(theta) * sin(phi)
            , self.longitud * cos(theta) ]

    def sistema(self):
        """Sistemas de ecuaciones que se pueden utilizar. Cada sistema debe devolver
        una lista correspondiente de las derivadas de los valores que contiene y"""
                
        def C01(y):
            """Sistema original derivado por Gatica."""

            # Definiendo nuestro modelo
            # theta'' + 2*gamma*theta' + (omega**2 - phi'**2 * cos(theta)) * sin(theta) = 0
            # phi'' * sin(theta) + 2*phi'*(theta' * cos(theta) + gamma * sin(theta)) = 0

            # Expresando en términos de ecuaciones de primer grado
            # theta' = thetaPrima
            # phi' = phiPrima
            # thetaPrima'   = -2 * gamma * thetaPrima - (omega**2 - phiPrima**2 * cos(theta)) * sin(theta)
            # phiPrima'     = -2 * phiPrima * (thetaPrima * cos(theta) + gamma * sin(theta)) / sin(theta)
            theta, phi, thetaPrima, phiPrima = y    # Notar el orden
            return [ thetaPrima, phiPrima
                , -2 * self.gamma * thetaPrima - (self.omega**2 - phiPrima**2 * cos(theta)) * sin(theta)
                , -2 * phiPrima * (thetaPrima * cos(theta) + self.gamma * sin(theta)) / sin(theta) ]

        def sinFriccion(y):
            theta, phi, thetaPrima, phiPrima = y    # Notar el orden
            return [ thetaPrima, phiPrima
                , (self.longitud * phiPrima**2 * sin(theta) * cos(theta) - self.g * sin(theta))/self.longitud
                , (-2 * phiPrima * thetaPrima * cos(theta))/sin(theta) ]

        return { 'C01': C01
            , 'sinFriccion': sinFriccion }

    def ecuaciones(self, y, t):
        """Escoger un sistema de ecuaciones y devolverlo."""
        return self.sistema()[self.sistemaEq](y)  # dydt

class Script():
    def main(self):
        # definiendo constantes
        b = 0.01
        g = 9.8

        modelo = Pendulo(b,g)

        y0  = [ 0.8*PI, -0.4*PI, 0.0*PI, 0.4*PI ]  # Condiciones iniciales
        t   = np.linspace(0, 40, 600)           # Dimensión temporal

        sol = odeint(modelo.ecuaciones, y0, t)

        # Agregando coordenadas rectangulares a los resultados
        conRects = [ row + modelo.coordenadasRectangulares(row[0], row[1]) for row in sol.tolist() ] 
        sol = np.array(conRects)

        print(sol)

        # Graficar la proyección x-y
        # plt.plot(sol[:, 4], sol[:, 5], 'b', label='Plano XY')

        # Graficar la proyección x-z
        # plt.plot(sol[:, 4], sol[:, 6], 'g', label='Plano XZ')

        # # Graficar la proyección y-z
        # plt.plot(sol[:, 5], sol[:, 6], 'y', label='Plano YZ')

        # Graficar theta vs tiempo
        # plt.plot(t, sol[:, 0], 'b', label='theta(t)')
        # plt.plot(t, sol[:, 2], 'g', label='theta\'(t)')

        # Graficar phi vs tiempo
        # plt.plot(t, sol[:, 1], 'b', label='phi(t)')
        # plt.plot(t, sol[:, 3], 'g', label='phi\'(t)')

        # Diagrama de fase de theta
        # plt.plot(sol[:, 0], sol[:, 2], 'g', label='Diagrama de Fase Theta')

        # Diagrama de fase de phi
        plt.plot(sol[:, 1], sol[:, 3], 'g', label='Diagrama de Fase Phi')

        plt.legend(loc='best')
        plt.xlabel('t')
        plt.grid()
        fig = plt.gcf()
        fig.savefig('spherical.png')


################### Defining tests
class TestScript(unittest.TestCase):
    def test_Constructor(self):
        self.assertEqual( type(Script()), Script )


################### Launch the script
if __name__ == '__main__':
    ENV = yaml.load( open(".env", 'r') )
    Script().main()