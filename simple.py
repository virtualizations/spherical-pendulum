#!/usr/bin/env python

import yaml
import unittest

import numpy as np
from numpy import sin

from scipy.integrate import odeint

import matplotlib.pyplot as plt
from matplotlib.pyplot import plot

# Definiendo nuestro modelo
# theta''(t) + b*theta'(t) + c*sin(theta(t)) = 0 

# Expresando en términos de ecuaciones de primer grado
# theta'(t) = omega(t)
# omega'(t) = -b*omega(t) - c*sin(theta(t))

def pendulo(y, t, b, c):
    theta, omega = y
    dydt = [ omega, -b*omega - c*sin(theta) ]

    return dydt

class Script():
    def main(self):
        # definiendo constantes
        b = 0.25
        c = 5.0

        y0  = [ np.pi - 0.1, 0.0 ]      # Condiciones iniciales
        t   = np.linspace(0, 10, 101)   # Dimensión temporal

        sol = odeint(pendulo, y0, t, args=(b, c))

        plt.plot(t, sol[:, 0], 'b', label='theta(t) [Ángulo]')
        plt.plot(t, sol[:, 1], 'g', label='omega(t) [Velocidad Angular]')
        plt.legend(loc='best')
        plt.xlabel('t')
        plt.grid()
        fig = plt.gcf()
        fig.savefig('demo.png', bbox_inches='tight')


################### Defining tests
class TestScript(unittest.TestCase):
    def test_Constructor(self):
        self.assertEqual( type(Script()), Script )


################### Launch the script
if __name__ == '__main__':
    ENV = yaml.load( open(".env", 'r') )
    Script().main()