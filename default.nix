with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "env";
  env = buildEnv { name = name; paths = buildInputs; };
  buildInputs = [

    # Comienzan dependencias

    # dependencias de desarrollo
    git
    gitAndTools.gitflow


    # paquetes generales de desarrollo en python
    python36
    python36Packages.pyyaml
    python36Packages.unittest2
    python36Packages.watchdog


    # paquetes específicos del proyecto
    python36Packages.pygame
    python36Packages.numpy
    python36Packages.scipy
    python36Packages.matplotlib

    # Finalizan dependencias
  ];
}
